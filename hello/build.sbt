name := "hello"

version := "1.0"

scalaVersion := "2.11.4"

libraryDependencies += "org.apache.derby" % "derby" % "10.4.1.3"

libraryDependencies += "org.codehaus.jackson" % "jackson-core-asl" % "1.6.1"

libraryDependencies += "org.scala-tools.testing" % "specs_2.8.0" % "1.6.5" % "test"

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

lazy val hello = taskKey[Unit]("Prints 'Hello World'")

lazy val sampleTask = taskKey[Int]("A sample task.")

lazy val sampleTask2 = taskKey[Int]("A sample task.")

lazy val intTask = taskKey[Int]("An int task")

lazy val stringTask = taskKey[String]("A string task")

hello := println("hello world!")

intTask := 1 + 2

stringTask := System.getProperty("user.name")

sampleTask := {
   val sum = 1 + 2
   println("sum: " + sum)
   sum
}

